import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';
import './chart_bar.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions);

  List<Map<String, Object>> get groupedTransactionValues {
    var tabWeek =  List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      var totalSum = 0.0;

      for (var i = 0; i < recentTransactions.length; i++) {
        var extractDate = recentTransactions[i].date;

        if (extractDate.day == weekDay.day &&
            extractDate.month == weekDay.month &&
            extractDate.year == weekDay.year) {
          totalSum += recentTransactions[i].amount;
        }
      }

      var dateFormatFr = DateFormat.E("fr").format(weekDay);

      return {
        'day': dateFormatFr.substring(0, dateFormatFr.length - 1),
        'amount': totalSum,
      };
    }).reversed.toList();

    var finalIndex = tabWeek[tabWeek.length - 1];
    tabWeek.removeAt(tabWeek.length - 1);
    tabWeek.insert(0, finalIndex);

    return tabWeek;

    
  }

  double get totalSpendingInWeek {
    print(groupedTransactionValues);
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: groupedTransactionValues.map((data) {
            return ChartBar(
              data['day'],
              data['amount'],
              totalSpendingInWeek == 0.0 ? 0.0 : (data['amount'] as double) / totalSpendingInWeek,
            );
          }).toList(),
        ),
      ),
    );
  }
}
