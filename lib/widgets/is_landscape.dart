import 'package:expenses_app/widgets/chart.dart';
import 'package:expenses_app/widgets/transaction_list.dart';
import 'package:flutter/material.dart';

class IsLandscape extends StatefulWidget {
  final MediaQueryData mediaQuery;
  final PreferredSizeWidget appBar;
  final recentTransaction;
  final userTransactions;
  final deleteTransaction;

  IsLandscape(
      {this.appBar,
      this.mediaQuery,
      this.recentTransaction,
      this.userTransactions,
      this.deleteTransaction});

  @override
  _IsLandscapeState createState() => _IsLandscapeState();
}

class _IsLandscapeState extends State<IsLandscape> {
  bool showChart = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: (widget.mediaQuery.size.height -
                  widget.appBar.preferredSize.height -
                  widget.mediaQuery.padding.top) *
              0.10,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Switch.adaptive(
                activeColor: Theme.of(context).accentColor,
                value: showChart,
                onChanged: (valSwitch) {
                  setState(() {
                    showChart = valSwitch;
                  });
                  print(showChart);
                },
              ),
              FittedBox(
                child: Text(
                  'Voir le graphique',
                  style: Theme.of(context).textTheme.title,
                ),
              ),
            ],
          ),
        ),
        if (showChart)
          Container(
            height: (widget.mediaQuery.size.height -
                    widget.appBar.preferredSize.height -
                    widget.mediaQuery.padding.top) *
                0.7,
            child: Chart(widget.recentTransaction),
          )
        else
          Container(
            height: (widget.mediaQuery.size.height -
                    widget.appBar.preferredSize.height -
                    widget.mediaQuery.padding.top) *
                0.9,
            child: TransactionList(
                widget.userTransactions, widget.deleteTransaction),
          )
      ],
    );
  }
}
