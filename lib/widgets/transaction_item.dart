import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';

class TransactionItem extends StatefulWidget {
  final Transaction transaction;
  final Function deleteTransactionId;

  const TransactionItem({
    Key key,
    @required this.transaction,
    @required this.deleteTransactionId,
  }) : super(key: key);

  @override
  _TransactionItemState createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {
  Color _bgColor;
  @override
  void initState() {
    super.initState();
    const availableColors = [
      Colors.blue,
      Colors.red,
      Colors.green,
    ];

    _bgColor = availableColors[Random().nextInt(3)];
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(vertical: 8),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: _bgColor,
          radius: 30.0,
          child: Padding(
            padding: EdgeInsets.all(6),
            child: FittedBox(
              child: Text(
                '${widget.transaction.amount.toStringAsFixed(2)}€',
              ),
            ),
          ),
        ),
        title: Text(
          widget.transaction.name,
          style: Theme.of(context).textTheme.title,
        ),
        subtitle: Text(
          DateFormat.yMMMd('fr').format(widget.transaction.date),
        ),
        trailing: MediaQuery.of(context).size.width > 460
            ? FlatButton.icon(
                label: Text('Supprimer'),
                textColor: Theme.of(context).errorColor,
                icon: Icon(Icons.delete),
                onPressed: () =>
                    widget.deleteTransactionId(widget.transaction.id),
              )
            : IconButton(
                tooltip: 'Supprimer',
                icon: Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                onPressed: () =>
                    widget.deleteTransactionId(widget.transaction.id),
              ),
      ),
    );
  }
}
