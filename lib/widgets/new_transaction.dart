import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './adaptive_flat_button.dart';

class NewTransaction extends StatefulWidget {
  final Function registerTransaction;

  NewTransaction(this.registerTransaction);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleInput = TextEditingController();
  final _amountInput = TextEditingController();
  var _selectedDate = DateTime.now();

  void _submitData() {
    if (_amountInput.text.isEmpty) {
      return;
    }

    final titleTx = _titleInput.text;
    final amountTx = double.parse(_amountInput.text);
    final selectedDateTx = _selectedDate;

    if (titleTx.isEmpty || amountTx <= 0) {
      return;
    }

    widget.registerTransaction(titleTx, amountTx, selectedDateTx);

    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime(2021),
    ).then((DateTime pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(  
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(top: 10, right: 10, left: 10, bottom: MediaQuery.of(context).viewInsets.bottom + 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(labelText: 'Titre'),
                controller: _titleInput,
                onSubmitted: (_) => _submitData(),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Montant'),
                controller: _amountInput,
                keyboardType: TextInputType.number,
                onSubmitted: (_) => _submitData(),
              ),
              Container(
                margin: EdgeInsets.only(top: 20, bottom: 50),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        _selectedDate == null
                            ? "Date sélectionnée : ${DateFormat.yMMMd('fr').format(DateTime.now())}"
                            : "Date sélectionnée : ${DateFormat.yMMMd('fr').format(_selectedDate)}",
                      ),
                    ),
                    Container(
                      height: 30,
                      child: VerticalDivider(
                        color: Colors.black,
                      ),
                    ),
                    AdaptiveFlatButton('Choisir une Date', _presentDatePicker)
                  ],
                ),
              ),
              RaisedButton(
                child: Text('Enregister'),
                color: Theme.of(context).primaryColor,
                textColor: Theme.of(context).textTheme.button.color,
                onPressed: _submitData,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
