import 'package:flutter/material.dart';

import './transaction_item.dart';
import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTransactionId;

  TransactionList(this.transactions, this.deleteTransactionId);

  @override
  Widget build(BuildContext context) {
    return transactions.isEmpty
        ? LayoutBuilder(
            builder: (ctx, constraints) {
              return Column(
                children: <Widget>[
                  Text(
                    'Aucunes dépenses... !',
                    style: Theme.of(context).textTheme.title,
                  ),
                  SizedBox(height: 30),
                  Container(
                    height: constraints.maxHeight * 0.70,
                    child: Image.asset(
                      'assets/images/waiting.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              );
            },
          )
        : ListView.builder(
            itemCount: transactions.length,
            itemBuilder: (BuildContext context, int index) {
              return TransactionItem(
                transaction: transactions[index],
                deleteTransactionId: deleteTransactionId,
              );
            },
          );
  }
}
