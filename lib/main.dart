//import 'dart:io';

import 'package:expenses_app/widgets/is_landscape.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/services.dart'; pour SystemChrome
import 'package:intl/date_symbol_data_local.dart';

import './widgets/new_transaction.dart';
import './models/transaction.dart';
import './widgets/transaction_list.dart';
import './widgets/chart.dart';

//TODOFor build //!\\ Change the check platform : "Theme.of(context).platform == TargetPlatform.iOS" <= TO => "Plateform.isIOS";
//TODOFor build //!\\ Uncomment: //build import:io;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
/*     SystemChrome.setPreferredOrientations([ // permet de forcer l'utilisateur a utilisé l'appli en mode portrait
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]); */
    initializeDateFormatting();
    return MaterialApp(
      title: 'Gestionnaire de dépenses',
      theme: ThemeData(
        // permet de centralisé le style de l'app
        primarySwatch: Colors.purple, // defini une couleur principale
        accentColor: Colors
            .amber, // seconde couleur, utilisé par défaut pour les float button
        fontFamily: 'Quicksand',
        textTheme: TextTheme(
          /* for all title */
          title: TextStyle(
            fontFamily: 'OpenSans',
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
          button: TextStyle(color: Colors.white),
        ),
        appBarTheme: AppBarTheme(
          /* for title in appBar */
          textTheme: TextTheme(
            title: TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        /* appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
        ), */
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //Stockage de toutes les dépenses dans une liste
  final List<Transaction> _userTransactions = [];

  List<Transaction> get _recentTransaction {
    // retourne les dernière transaction. Jour J => (J - 7)
    return _userTransactions.where((tx) {
      return tx.date.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _addNewTransaction(
      String nameTx, double amountTx, DateTime selectedDateTx) {
    // consturit en fonction de la saisie du user un nouvelle dépense
    final newTx = Transaction(
      name: nameTx,
      amount: amountTx,
      date: selectedDateTx,
      id: DateTime.now().toString(),
    );

    setState(() {
      _userTransactions.add(newTx);
    });
  }

  void _startAddNewTransaction(BuildContext ctx) {
    // permet d'ouvir le formulaire de bas en haut pour l'envoie d'une dépense.
    showModalBottomSheet(
      context: ctx,
      isScrollControlled: true,
      builder: (_) {
        return NewTransaction(_addNewTransaction);
      },
    );
  }

  void _deleteTransaction(String id) {
    setState(() {
      _userTransactions.removeWhere((item) => item.id == id);
    });
  }

  List<Widget> _isPortrait(
      MediaQueryData mediaQuery, PreferredSizeWidget appBar) {
    return [
      Container(
        height: (mediaQuery.size.height -
                appBar.preferredSize.height -
                mediaQuery.padding.top) *
            0.3,
        child: Chart(_recentTransaction),
      ),
      Container(
        height: (mediaQuery.size.height -
                appBar.preferredSize.height -
                mediaQuery.padding.top) *
            0.7,
        child: TransactionList(_userTransactions, _deleteTransaction),
      )
    ];
  }

  PreferredSizeWidget _appBar() {
    return Theme.of(context).platform == TargetPlatform.iOS
        ? CupertinoNavigationBar(
            middle: Text('Gestionnaire de dépenses'),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  child: Icon(CupertinoIcons.add),
                  onTap: () => _startAddNewTransaction(context),
                )
              ],
            ),
          )
        : AppBar(
            title: Text('Gestionnaire de dépenses'),
            actions: <Widget>[
              IconButton(
                // un bouton mais a la place du text, un icon
                icon: Icon(Icons.add),
                onPressed: () => _startAddNewTransaction(context),
                color: Theme.of(context).accentColor,
              )
            ],
          );
  }

  Widget _bodyPage(MediaQueryData mediaQuery, isLandscape) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            if (isLandscape)
              IsLandscape(
                appBar: _appBar(),
                mediaQuery: mediaQuery,
                recentTransaction: _recentTransaction,
                userTransactions: _userTransactions,
                deleteTransaction: _deleteTransaction,
              ),
            if (!isLandscape) ..._isPortrait(mediaQuery, _appBar()),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final isLandscape = mediaQuery.orientation == Orientation.landscape;

    return Theme.of(context).platform == TargetPlatform.iOS
        ? CupertinoPageScaffold(
            navigationBar: _appBar(),
            child: _bodyPage(mediaQuery, isLandscape),
          )
        : Scaffold(
            appBar: _appBar(),
            body: _bodyPage(mediaQuery, isLandscape),
            floatingActionButton:
                Theme.of(context).platform == TargetPlatform.iOS
                    ? Container()
                    : FloatingActionButton(
                        // génère un bouton flottant pour Android
                        child: Icon(Icons.add),
                        onPressed: () => _startAddNewTransaction(context),
                      ),
            // localisation de ce bouton flottant sur le mobile
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          );
  }
}
